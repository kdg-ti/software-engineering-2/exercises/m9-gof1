Mogelijkheden om dit te runnen
1. via Intellij run configurations menu (links van het groene run pijltje),
selecteer menu item ```gof1_oefeningen:observer_2views [run]```
   run configuration: Gradle
    gradleproject:  observer_2views
   Task: run
    
2. Via terminal venster:
```
gradlew observer_2views: run
```