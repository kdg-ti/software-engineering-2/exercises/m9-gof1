package filesystem;

/**
 * @author Jan de Rijke.
 */
public interface Container extends Component{
	 void add(Component c) ;
	 void remove(Component c) ;
}
