package filesystem;

/* Hier niets veranderen!
*/

public interface Component {

  long getSize();

  String getPath();

  void setParent(Container parent);

}