package filesystem;

import java.util.*;

public  class Directory implements Container {
	private final String name;
	private final List<Component> children;
	private Container parent;

	public Directory(String name) {
		this.name = name;
		this.parent = null;
		children = new ArrayList<Component>();
	}

	@Override
	public long getSize() {
		// TODO: implementeer methode

		return 0;
	}

	@Override
	public String getPath() {
		// TODO: implementeer methode
		return null;
	}

	@Override
	public void setParent(Container parent) {
		// TODO: implementeer methode

	}

	public void add(Component c) {
		// TODO: implementeer methode

	}

	public void remove(Component c) {
		// TODO: implementeer methode
	}

	@Override
	public String toString() {
		// TODO: implementeer methode
		return super.toString();
	}

	public List<Component> list() {
		return children;
	}
}
