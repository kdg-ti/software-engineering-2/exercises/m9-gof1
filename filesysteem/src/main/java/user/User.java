package user;

import java.util.LinkedList;
import java.util.List;

public class User {
	private String name;
	private List<String> messages=new LinkedList<>();

	public User(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addMessage(String message) {
		// LIFO
		messages.addFirst(message);
	}
	public List<String> getMessages() {
		return messages;
	}
}
